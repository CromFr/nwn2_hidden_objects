#include "hidden_object_inc"

void main()
{
	object oPC = GetExitingObject();
	object oArea = GetArea(OBJECT_SELF);

	if(GetIsPC(oPC)){
		int bTriggerEmpty = TRUE;

		object oNearPC = GetFirstPC();
		while(GetIsObjectValid(oNearPC))
		{
			if(GetArea(oNearPC) == oArea && GetIsInSubArea(oPC, OBJECT_SELF)){
				bTriggerEmpty = FALSE;
				break;
			}
			oNearPC = GetNextPC();
		}

		if(bTriggerEmpty)
			HiddenObject_Hide(OBJECT_SELF);
	}
}