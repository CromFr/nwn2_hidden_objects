# Hidden objects / secrets passages system for Neverwinter Nights 2

## Trying

Copy all these files into `Documents\Neverwinter Nights 2\modules\hidden_objects`, launch the game and start this module as a new single player module.

## Install

Copy those files to your module folder:
- `hidden_object_inc.nss`
- `hidden_object_onenter.nss`
- `hidden_object_onexit.nss`
- `hidden_object_trigger.utt`

## Usage
See instructions in [hidden_object_inc.nss](hidden_object_inc.nss)