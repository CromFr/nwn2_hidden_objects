// Hidden objects / secrets passages system
// by CromFr / Thibaut CHARLES
//
// This is a set of scripts to create completely hidden secrets that the
// player reveals when walking in a trigger and succeeding a specific skill
// roll. The secret is automatically hidden when the player walks out of the
// trigger. Once the secret has been revealed once to the player (either by
// doing a skill check of walking in the trigger while the secret is
// revealed), the player can reveal the secret without doing any skill check.
//
// Local variables on the trigger:
// - objects_tag (string): Tag of the objects to become interactable.
//   Placeable will become useable, doors will be unlocked, and lights will be
//   switched on.
// - decor_tag (string): Tag of the placeable objects to remove. Each
//   placeable must be non static, and have an associated blueprint that is
//   non static. Object location and scale are kept, but colors and properties
//   must match the template to be re-created correctly.
// - sef (string, optional): SEF effect to play on the decor placeables that
//   will be removed when the secret is revealed. Defaults to
//   "sp_illusion_aoe".
// - sef_obj (string, optional): SEF effect to play on the object placeables
//   that will become interactable when the secret is revealed. Leave empty
//   for no effect.
// - skill (int, optional): Skill to check integer constant for revealing the
//   secret. Defaults to 14 (for SKILL_SEARCH)
// - dc (int): DC for the skill check.
// - feedback (int, optional): If 1, the player will see the skill check being
//   rolled, and the DC to reveal the secret. Defaults to 0 (false).
// - found_script (string optional): Script to call when the player has
//   discovered the secret for the first time. Can be used for example for
//   rewarding the player. The script must be a `void main(object
//   oPlayer){...}`. Defaults to no script being called.


// If this value is set, the information for whether or not the player knows
// the secret will be stored in a persistent campaign database named
// HIDDEENOBJECT_CFG_CAMPAIGN. If this value is not set, the information will
// be stored as a local variable on the trigger, and will lost after a server
// reboot.
const string HIDDEENOBJECT_CFG_CAMPAIGN = "";



// Check if the secret is hidden or visible
int HiddenObject_IsShown(object oTrigger){
	return GetLocalInt(OBJECT_SELF, "_shown");
}

// Make the secret visible
void HiddenObject_Show(object oTrigger){
	if(HiddenObject_IsShown(OBJECT_SELF))
		return;
	SetLocalInt(oTrigger, "_shown", TRUE);

	int iIndex;

	// Make object useable
	string sObjectsTag = GetLocalString(oTrigger, "objects_tag");
	if(sObjectsTag != ""){
		string sSef = GetLocalString(oTrigger, "sef_obj");

		iIndex = 0;
		object oObject = GetObjectByTag(sObjectsTag, iIndex);
		while(GetIsObjectValid(oObject)){
			switch(GetObjectType(oObject)){
				case OBJECT_TYPE_PLACEABLE:
					SetUseableFlag(oObject, TRUE);
					break;
				case OBJECT_TYPE_DOOR:
					SetLocked(oObject, FALSE);
					break;
				case OBJECT_TYPE_LIGHT:
					SetLightActive(oObject, TRUE);
					break;
			}

			if(sSef != "")
				ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectNWN2SpecialEffectFile(sSef), oObject);

			oObject = GetObjectByTag(sObjectsTag, ++iIndex);
		}
	}

	// Hide decor
	string sDecorTag = GetLocalString(oTrigger, "decor_tag");
	if(sDecorTag != ""){
		string sSef = GetLocalString(oTrigger, "sef");
		if(sSef == "") sSef = "sp_illusion_aoe";

		iIndex = 0;
		object oDecor = GetObjectByTag(sDecorTag, iIndex);
		while(GetIsObjectValid(oDecor)){
			// Save placeable properties
			string sI = IntToString(iIndex);
			SetLocalString(oTrigger, "_decor_resref_" + sI, GetResRef(oDecor));
			SetLocalLocation(oTrigger, "_decor_loc_"+sI, GetLocation(oDecor));
			SetLocalFloat(oTrigger, "_decor_scalex_"+sI, GetScale(oDecor, SCALE_X));
			SetLocalFloat(oTrigger, "_decor_scaley_"+sI, GetScale(oDecor, SCALE_Y));
			SetLocalFloat(oTrigger, "_decor_scalez_"+sI, GetScale(oDecor, SCALE_Z));

			vector vVfx = GetPosition(oDecor);
			vVfx.z = GetPosition(oTrigger).z + 0.1;
			ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectNWN2SpecialEffectFile(sSef), Location(GetArea(oTrigger), vVfx, 0.0));

			DestroyObject(oDecor);

			oDecor = GetObjectByTag(sDecorTag, ++iIndex);
		}

		SetLocalInt(oTrigger, "_decor_count", iIndex);
	}
}

// Hide the secret
void HiddenObject_Hide(object oTrigger){
	if(!HiddenObject_IsShown(oTrigger))
		return;
	SetLocalInt(oTrigger, "_shown", FALSE);

	string sSef = GetLocalString(oTrigger, "sef");
	if(sSef == "") sSef = "sp_illusion_aoe";

	int iIndex;

	// Make object non-useable
	string sObjectsTag = GetLocalString(oTrigger, "objects_tag");
	if(sObjectsTag != ""){
		string sSef = GetLocalString(oTrigger, "sef_obj");

		iIndex = 0;
		object oObject = GetObjectByTag(sObjectsTag, iIndex);
		while(GetIsObjectValid(oObject)){
			switch(GetObjectType(oObject)){
				case OBJECT_TYPE_PLACEABLE:
					SetUseableFlag(oObject, FALSE);
					break;
				case OBJECT_TYPE_DOOR:
					SetLocked(oObject, TRUE);
					if(GetIsOpen(oObject))
						AssignCommand(oObject, ActionCloseDoor(oObject));
					break;
				case OBJECT_TYPE_LIGHT:
					SetLightActive(oObject, FALSE);
					break;
			}

			if(sSef != "")
				ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectNWN2SpecialEffectFile(sSef), oObject);

			oObject = GetObjectByTag(sObjectsTag, ++iIndex);
		}
	}

	// Show decor
	string sDecorTag = GetLocalString(oTrigger, "decor_tag");
	if(sDecorTag != ""){
		string sSef = GetLocalString(oTrigger, "sef");
		if(sSef == "") sSef = "sp_illusion_aoe";

		int nCount = GetLocalInt(oTrigger, "_decor_count");
		for(iIndex = 0 ; iIndex < nCount ; iIndex++){
			/*Code*/			string sI = IntToString(iIndex);
			string sDecorResref = GetLocalString(oTrigger, "_decor_resref_" + sI);
			location lDecor = GetLocalLocation(oTrigger, "_decor_loc_" + sI);
			vector vDecorScale = Vector(
				GetLocalFloat(oTrigger, "_decor_scalex_" + sI),
				GetLocalFloat(oTrigger, "_decor_scaley_" + sI),
				GetLocalFloat(oTrigger, "_decor_scalez_" + sI)
			);

			object oDecor = CreateObject(OBJECT_TYPE_PLACEABLE, sDecorResref, lDecor, TRUE, sDecorTag);
			if(!GetIsObjectValid(oDecor)){
				WriteTimestampedLogEntry("Cannot re-create hidden object decor placeable resref='"+sDecorResref+"' for trigger '"+GetTag(oTrigger)+"' in area '"+GetName(GetArea(oTrigger))+"'");
			}
			SetScale(oDecor, vDecorScale.x, vDecorScale.y, vDecorScale.z);

			vector vVfx = GetPosition(oDecor);
			vVfx.z = GetPosition(oTrigger).z + 0.1;
			ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectNWN2SpecialEffectFile(sSef), Location(GetArea(oTrigger), vVfx, 0.0));
		}
	}
}


// ==============

// Internal function
int _HiddenObject_SkillRoll(object oTrigger, object oPC){
	int nDC = GetLocalInt(OBJECT_SELF, "dc");
	int nSkill = GetLocalInt(OBJECT_SELF, "skill");
	int bFeedback = GetLocalInt(OBJECT_SELF, "feedback");
	if(nSkill == 0) nSkill = SKILL_SEARCH;

	return GetIsSkillSuccessful(oPC, nSkill, nDC, bFeedback);
}

// Internal function
string _HiddenObject_TriggerID(object oTrigger){
	vector vPos = GetPosition(oTrigger);
	return GetResRef(GetArea(oTrigger)) + "#" + FloatToString(vPos.x, 18, 1) + "#" + FloatToString(vPos.y, 18, 1);
}

// Internal function
void _HiddenObject_RegisterPC(object oTrigger, object oPC){
	int bKnown;
	if(HIDDEENOBJECT_CFG_CAMPAIGN != ""){
		bKnown = GetCampaignInt(HIDDEENOBJECT_CFG_CAMPAIGN, "found_"+_HiddenObject_TriggerID(oTrigger), oPC);
	}
	else{
		bKnown = GetLocalInt(oTrigger, "_found_" + ObjectToString(oPC));
	}

	if(!bKnown){
		string sFoundScript = GetLocalString(oTrigger, "found_script");
		if(sFoundScript != ""){
			ClearScriptParams();
			AddScriptParameterObject(oPC);
			ExecuteScriptEnhanced(sFoundScript, oTrigger);
		}

		if(HIDDEENOBJECT_CFG_CAMPAIGN != ""){
			SetCampaignInt(HIDDEENOBJECT_CFG_CAMPAIGN, "found_"+_HiddenObject_TriggerID(oTrigger), TRUE, oPC);
		}
		else{
			SetLocalInt(oTrigger, "_found_" + ObjectToString(oPC), TRUE);
		}
	}

}