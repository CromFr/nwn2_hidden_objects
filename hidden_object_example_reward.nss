
void main(object oPC)
{
	object oTrigger = OBJECT_SELF;

	FloatingTextStringOnCreature("You just discovered a new secret !", oPC, FALSE);
	AssignCommand(oPC, PlaySound("as_pl_tavclap2"));
	GiveXPToCreature(oPC, 1000);
}