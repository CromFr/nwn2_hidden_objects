#include "hidden_object_inc"

void main()
{
	object oPC = GetEnteringObject();

	if(GetIsPC(oPC)){
		if(HiddenObject_IsShown(OBJECT_SELF)){
			_HiddenObject_RegisterPC(OBJECT_SELF, oPC);
		}
		else{
			if(_HiddenObject_SkillRoll(OBJECT_SELF, oPC)){
				_HiddenObject_RegisterPC(OBJECT_SELF, oPC);
				HiddenObject_Show(OBJECT_SELF);
			}
		}
	}
}